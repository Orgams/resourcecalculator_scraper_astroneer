
#logger
import argparse
import logging
import logging.handlers
import sys

import os

#request
import requests
import requests_cache

# scraping
import urllib
import bs4

# scraper humain
import pandas as pd
from fake_useragent import UserAgent
import time
import random

#autre
from bs4 import Tag, NavigableString
import yaml
import itertools as it


"""
	Create a config file and image linked for project RessourcesCalculator for game Astroneers
"""

proxies = pd.read_csv('proxy_list.txt', header = None)
proxies = proxies.values.tolist()
proxies = list(it.chain.from_iterable(proxies))
proxy_pool = it.cycle(proxies)
proxy = next(proxy_pool)

logger = logging.getLogger(os.path.splitext(os.path.basename(sys.argv[0]))[0])

requests_cache.install_cache('astroneer_wiki_cache', backend='sqlite', expire_after=360000)

ua = UserAgent()

## scraper les resources
config = {
	"authors" : {"org_AMS" : ""},
	"index_page_display_name" : "Astroneer",
	"recipe_types" : {},
	"resources" : {}
}

url_base = "https://astroneer.gamepedia.com"

recipe_types = set()

allready_process = set()

def requests_get(url):
	logger.debug("requests_get : {0}".format(url))
	proxy = next(proxy_pool)
	response = requests.get(url, proxies={"http": proxy, "https": proxy}, headers={'User-Agent': ua.random})
	if not requests_cache.has_url(url):
		time.sleep(random.randrange(1,5))
	logger.debug("Used Cache : {0}, response status : {1}".format(category, response.from_cache, response.status_code))
	return response

def get_links_from_category_page(category, class_category="mw-category"):
	# recuperer les liens des pages de resource rafiné
	category_response = requests_get(url_base+"/Category:"+category)
	category_page = bs4.BeautifulSoup(category_response.text, 'html.parser')
	#remove Template special page
	links_prefilter = category_page.find("div", {"class":class_category}).find_all("a")
	links = list(filter(lambda link: (link.text.find('Template') == -1), links_prefilter))
	return links

def get_img(resource_cible, resources_page=None):
	path_img = "./images/"+resource_cible.lower().replace("[^a-zA-Z]", "")+".png"
	if not os.path.exists(path_img):
		url_img = url_base+"/"+resource_cible
		logger.debug("Download image {} as {}".format(url_img, path_img))

		if resources_page == None:
			resources_resp = requests_get(url_img)
			resources_page = bs4.BeautifulSoup(resources_resp.text, 'html.parser')

		image_link = resources_page.find("table", {"class":"infoboxtable"}).find("a", {"class":"image"})

		if not image_link == None:
			image_url = image_link.find("img")["src"]
			urllib.request.urlretrieve(image_url, path_img)

def add_config_for_simple_resources(category, class_category="mw-category"):
	links = get_links_from_category_page(category=category, class_category=class_category)
	for link in links:
		#recuperer le nom de la resource
		resource_cible = link["href"][1:]
		logger.debug("Resource cible process : {0}".format(resource_cible))

		# eviter de retaiter une resouce
		if resource_cible in allready_process:
			continue

		#Copier et personalise le template de resource naturelle
		resource = {
			'recipes': [
				{
					'output' : 1,
					'recipe_type' : 'Raw Resource',
					'requirements' : {
					}
				}
			]
		}

		#add to config
		resource["recipes"][0]["requirements"][resource_cible] = 0
		config["resources"][resource_cible] = resource
		allready_process.add(resource_cible)

		#dl image of resource

		# get image if new
		get_img(resource_cible=resource_cible)

def add_config_for_recipe_resources(category, class_category="mw-category"):
	links = get_links_from_category_page(category=category, class_category=class_category)
	for link in links:

		#recuperer le nom de la resource
		resource_cible = link["href"][1:]
		logger.debug("Resource cible process : {0}".format(resource_cible))

		# eviter de retaiter une resouce
		if resource_cible in allready_process:
			continue

		# get datas
		resources_resp = requests_get(url_base+"/"+resource_cible)
		resources_page = bs4.BeautifulSoup(resources_resp.text, 'html.parser')
		recipeTable = resources_page.find("table", {"class":"recipeTable"})
		if recipeTable == None:
			continue
		data = recipeTable.find_all("tr")[1].find_all(['th','td'])
		inputs = data[1]
		module = data[2]

		# Init template
		resources = {
			'recipes': [
				{
					'output' : 1,
					'recipe_type' : 'Raw Resource',
					'requirements' : {
					}
				}
			]
		}

		# Add all of input resources
		for input in inputs:
			if isinstance(input, Tag) and input.name == "span":
				resource_input = input.find("a")["href"][1:]
				resources["recipes"][0]["requirements"][resource_input] = -1
			if isinstance(input, NavigableString):
				input = input.strip()
				if input[:1] == 'x':
					nb = int(input[1:])
					resources["recipes"][0]["requirements"][resource_input] = -nb

		# Add type of recipe
		recipe_type = module.find('a')["href"][1:]
		recipe_types.add(recipe_type)

		#add to config
		resources["recipes"][0]["recipe_type"] = recipe_type
		config["resources"][resource_cible] = resources
		allready_process.add(resource_cible)

		# get image if new
		get_img(resource_cible=resource_cible, resources_page=resources_page)

def process_categorie(category, fonc_process, class_category="mw-category"):
	logger.info("Process category {}".format(category))
	fonc_process(category=category, class_category=class_category)

def make_parse_args():
	"""Parse arguments."""
	parser = argparse.ArgumentParser(description=sys.modules[__name__].__doc__)

	g = parser.add_mutually_exclusive_group()
	g.add_argument("--debug", "-d", action="store_true",
					default=False,
					help="enable debugging")
	g.add_argument("--silent", "-s", action="store_true",
					default=False,
					help="don't log to console")

	return parser

def setup_logging(options):
	"""Configure logging."""
	root = logging.getLogger("")
	root.setLevel(logging.WARNING)
	logger.setLevel(options.debug and logging.DEBUG or logging.INFO)
	if not options.silent:
		if not sys.stderr.isatty():
			facility = logging.handlers.SysLogHandler.LOG_DAEMON
			sh = logging.handlers.SysLogHandler(address='/dev/log', facility=facility)
			sh.setFormatter(logging.Formatter("{0}[{1}]: %(message)s".format(logger.name, os.getpid())))
			root.addHandler(sh)
		else:
			ch = logging.StreamHandler()
			ch.setFormatter(logging.Formatter("%(levelname)s[%(name)s] %(message)s"))
			root.addHandler(ch)

if __name__ == "__main__":
	# Create parser for argumants
	parser = make_parse_args()

	# execute autocompete
	# argcomplete.autocomplete(parser)

	# Parse arguments
	options = parser.parse_args(sys.argv[1:])

	# Setup logging with option
	setup_logging(options)

	#
	# resource naturelles et atmospherique (resources simple)
	#

	# creer le block pour les resources naturelle et l'ajouter au "resources" dans le dico yaml
	process_categorie(category="Natural_resources", fonc_process=add_config_for_simple_resources)

	# creer le block pour les resources atmospherique et l'ajouter au "resources" dans le dico yaml
	process_categorie(category="Atmospheric_resources", class_category="mw-category-generated", fonc_process=add_config_for_simple_resources)

	#
	# resource composé et rafiné
	#

	# creer le block pour les resources composées et les ajouter au "resources" dans le dico yaml
	process_categorie(category="Composite_resources", fonc_process=add_config_for_recipe_resources)
	# creer le block pour les resources rafinées et les ajouter au "resources" dans le dico yaml
	process_categorie(category="Refined_resources", fonc_process=add_config_for_recipe_resources)

	#
	# Module
	#

	# creer le block pour les module xl et les ajouter au "resources" dans le dico yaml
	process_categorie(category="Extra_Large", fonc_process=add_config_for_recipe_resources)
	# creer le block pour les module l et les ajouter au "resources" dans le dico yaml
	process_categorie(category="Large", fonc_process=add_config_for_recipe_resources)
	# creer le block pour les module m et les ajouter au "resources" dans le dico yaml
	process_categorie(category="Medium", fonc_process=add_config_for_recipe_resources)
	#creer le block pour les module s et les ajouter au "resources" dans le dico yaml
	process_categorie(category="Small", fonc_process=add_config_for_recipe_resources)

	#
	# add part recipe type
	#
	for recipe_type in recipe_types:
		config["recipe_types"][recipe_type] = "Use a {0} to turn {{IN_ITEMS}} into {{OUT_ITEM}}".format(recipe_type.lower().replace("_", " "))

	# écrire le fichier resources.gen.yaml compatible avec ResourceCalculator
	with open(r"resources.gen.yaml", "w") as file:
		yaml.dump(config, file)

